import logging
from email.message import EmailMessage
import smtplib
from typing import List

from ..entry import Entry
from .notification_sender import NotificationSender, NotificationSenderError
from .utils import get_env

_ENTRY_TO_TEXT = "* Une nouvelle version de {name} est disponible : {version} ({url})"

logger = logging.getLogger(__name__)


class EmailSender(NotificationSender):
    """
    Sends an email with a list of updated softwares
    """

    def __init__(
        self,
        mailer_host: str,
        mailer_port: int,
        user: str,
        password: str,
        subject: str,
        to_address: str,
        from_address: str,
    ):
        self.mailer_host = mailer_host
        self.mailer_port = mailer_port
        self.user = user
        self.password = password
        self.subject = subject
        self.to_address = to_address
        self.from_address = from_address

    @classmethod
    def from_env(cls) -> "EmailSender":
        """
        Initializes an EmailSender using environment variables
        """

        host = get_env("MAIL_HOST")
        port = int(get_env("MAIL_PORT"))
        user = get_env("MAIL_USER")
        password = get_env("MAIL_PASSWORD")
        subject = get_env("MAIL_SUBJECT")
        to_address = get_env("MAIL_TO")
        from_address = get_env("MAIL_FROM")

        return cls(host, port, user, password, subject, to_address, from_address)

    @staticmethod
    def _format_entry(entry: Entry) -> str:
        return _ENTRY_TO_TEXT.format(
            name=entry.name, version=entry.version, url=entry.url
        )

    def notify_updates(self, entries: List[Entry]) -> None:
        content = "\n".join(map(EmailSender._format_entry, entries))

        msg = EmailMessage()
        msg.set_content(content)
        msg["Subject"] = self.subject
        msg["From"] = self.from_address
        msg["To"] = self.to_address

        try:
            with smtplib.SMTP(self.mailer_host, self.mailer_port) as smtp:
                smtp.starttls()
                smtp.ehlo()
                logging.debug("Connected to SMTP server")

                smtp.login(self.user, self.password)
                smtp.send_message(msg)
                logging.debug("Email successfully sent")

        except smtplib.SMTPConnectError as err:
            raise NotificationSenderError(
                f"Could not connect to SMTP server: {err}"
            ) from err
        except smtplib.SMTPRecipientsRefused as err:
            raise NotificationSenderError(
                "Could not send email: all recipients refused it"
            ) from err
        except TimeoutError as err:
            raise NotificationSenderError(
                "Could not connect to SMTP server: server did not reply in time"
            ) from err
        except ValueError as err:
            raise NotificationSenderError(
                "Could not send email: ValueError: {err}"
            ) from err
