import os

from .notification_sender import NotificationSenderError


def get_env(key: str) -> str:
    value = os.getenv(key)
    if value is None:
        raise NotificationSenderError(f"Missing environment variable {key}")
    return value
