import logging
from typing import Any, Dict, List

import requests

from ..entry import Entry
from .notification_sender import NotificationSender, NotificationSenderError
from .utils import get_env

logger = logging.getLogger(__name__)

DEFAULT_MATTERMOST_MAX_POST_LEN = 4000
"""
Defines the maximum size of a Mattermost post.

It can be up to 16383 unicode chars (65ko) since v5.0, and is configurable in
the config.json file of the Mattermost server.
"""

MESSAGES_TEMPLATE = (
    "* Une nouvelle version de {name} est disponible [{version}]({url}) !"
)


def paginate_message(message: str, separator: str, max_substr_len: int) -> List[str]:
    """
    Split a message into the smallest number of substr having a maximum length.

    The order of splits is kept. In case a single split is too big, it is also kept.

    :param message: Message to split using the separator
    :param separator: A char or str that splits
    """
    if message == "":
        return []

    substrings: List[str] = []
    substrings.append("")
    for substring in message.split(sep=separator):
        cur_len = len(substrings[-1])
        if cur_len + len(substring) + len(separator) > max_substr_len:
            substrings.append("")
        substrings[-1] += separator + substring

    return substrings


class MattermostNotifier(NotificationSender):
    """
    Sends a message in a Mattermost channel listing updated softwares
    """

    def __init__(
        self,
        mattermost_host: str,
        channel_id: str,
        token: str,
        max_posts_len: int = DEFAULT_MATTERMOST_MAX_POST_LEN,
    ):
        self.mattermost_host = mattermost_host
        self.channel_id = channel_id
        self.token = token
        self.max_posts_len = max_posts_len

    @classmethod
    def from_env(cls) -> "MattermostNotifier":
        """
        Initializes a MattermostNotifier using environment variables
        """
        return cls(
            get_env("MM_SERVER_HOST"), get_env("MM_CHANNEL_ID"), get_env("MM_TOKEN")
        )

    @staticmethod
    def _entry_to_message(entry: Entry) -> str:
        return MESSAGES_TEMPLATE.format(
            name=entry.name, version=entry.version, url=entry.url
        )

    def _get_api_root(self) -> str:
        return f"https://{self.mattermost_host}/api/v4"

    def notify_updates(self, entries: List[Entry]) -> None:
        req_headers: Dict[str, str] = {"Authorization": f"Bearer {self.token}"}
        raw_message = "\n".join(map(MattermostNotifier._entry_to_message, entries))
        messages = paginate_message(raw_message, "\n", self.max_posts_len)
        full_url = self._get_api_root() + "/posts"

        logger.debug(
            "Sending entries to mattermost server %s in %s posts",
            self.mattermost_host,
            len(messages),
        )

        for message in messages:
            req_data: Dict[str, Any] = {
                "channel_id": self.channel_id,
                "message": message,
            }
            try:
                response = requests.post(full_url, json=req_data, headers=req_headers)
            except TypeError as err:
                raise NotificationSenderError(
                    f"Could not serialize entries to Mattermost server {self.mattermost_host}: {err}"
                ) from err
            except requests.exceptions.ConnectionError as err:
                raise NotificationSenderError(
                    f"Could not post message to Mattermost server {self.mattermost_host}: {err}"
                ) from err

            if (
                # pylint: disable=no-member
                response.status_code
                != requests.codes.created
            ):
                raise NotificationSenderError(
                    f"Mattermost server {self.mattermost_host} replied with status={response.status_code} (content={response.content})"
                )
