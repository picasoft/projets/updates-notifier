from abc import abstractmethod, ABCMeta
from typing import List

from ..entry import Entry


class NotificationSenderError(RuntimeError):
    pass


class NotificationSender(metaclass=ABCMeta):
    @abstractmethod
    def notify_updates(self, entries: List[Entry]) -> None:
        pass
