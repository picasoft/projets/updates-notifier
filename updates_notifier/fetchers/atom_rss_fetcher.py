import logging
from typing import List

import defusedxml.ElementTree as ET
import requests

from ..entry import Entry
from .project_fetcher import ProjectFetcher

logger = logging.getLogger(__name__)

_ATOM_STANDARD = "{http://www.w3.org/2005/Atom}"
_ATOM_ENTRY = f"{_ATOM_STANDARD}entry"
_ATOM_TAG_ID = f"{_ATOM_STANDARD}id"
_ATOM_TAG_VERSION = f"{_ATOM_STANDARD}title"
_ATOM_TAG_URL = f"{_ATOM_STANDARD}link"


class AtomRSSFetcher(ProjectFetcher):
    def __init__(self, feed_id: int, name: str, stream_url: str):
        super().__init__(feed_id, name)

        self.stream_url = stream_url

    def fetch_entries(self) -> List[Entry]:
        req = requests.get(self.stream_url)
        if req.status_code != requests.codes.ok:
            logger.error(
                "Failed to get %s RSS file at %s, server returned %s.",
                self.name,
                self.stream_url,
                req.status_code,
            )
            return []

        entries: List[Entry] = []

        try:
            root = ET.fromstring(req.content)
            for entry in root.findall(_ATOM_ENTRY):
                tag_id = entry.findtext(_ATOM_TAG_ID)
                version = entry.findtext(_ATOM_TAG_VERSION)
                url = entry.find(_ATOM_TAG_URL).get("href")

                entries.append(Entry(self.feed_id, tag_id, self.name, version, url))

        except ET.ParseError as err:
            logger.error("Failed to parse XML for %s: %s", self.name, str(err))

        return entries
