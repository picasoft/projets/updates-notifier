from abc import ABCMeta, abstractmethod
from typing import List

from ..entry import Entry


class ProjectFetcher(metaclass=ABCMeta):
    def __init__(self, feed_id: int, name: str):
        self.feed_id = feed_id
        self.name = name

    @abstractmethod
    def fetch_entries(self) -> List[Entry]:
        pass
