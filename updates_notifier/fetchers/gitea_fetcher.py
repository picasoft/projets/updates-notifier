from .project_fetcher import ProjectFetcher
from .github_fetcher import GithubFetcher


class GiteaFetcher(GithubFetcher):
    def __init__(self, feed_id: int, name: str, gitea_host: str, repository: str):
        ProjectFetcher.__init__(self, feed_id, name)

        self.gitea_host = gitea_host
        self.repository = repository

    def _get_releases_url(self) -> str:
        return f"https://{self.gitea_host}/api/v1/repos/{self.repository}/releases"
