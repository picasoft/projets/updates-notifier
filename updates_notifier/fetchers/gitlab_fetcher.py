import logging
from typing import List
import urllib.parse

import requests

from ..entry import Entry
from .project_fetcher import ProjectFetcher

logger = logging.getLogger(__name__)


class GitLabFetcher(ProjectFetcher):
    def __init__(self, feed_id: int, name: str, gitlab_host: str, repository: str):
        super().__init__(feed_id, name)

        self.gitlab_host = gitlab_host
        self.repository = repository

    def _get_releases_url(self) -> str:
        return f"https://{self.gitlab_host}/api/v4/projects/{urllib.parse.quote(self.repository, safe='')}/releases"

    def fetch_entries(self) -> List[Entry]:
        req = requests.get(self._get_releases_url())
        if req.status_code != requests.codes.ok:
            logger.error(
                "Failed to get %s at %s, server returned %s.",
                self.name,
                self.repository,
                req.status_code,
            )
            return []

        entries: List[Entry] = []

        try:
            for remote_entry in req.json():
                tag_id = remote_entry["commit"]["id"]
                tag_name = remote_entry["name"]
                url = remote_entry["_links"]["self"]
                entries.append(Entry(self.feed_id, tag_id, self.name, tag_name, url))
        except requests.exceptions.JSONDecodeError as err:
            logger.error(
                "Failed to parse entries of project %s: %s", self.name, str(err)
            )
        return entries
