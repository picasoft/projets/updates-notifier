from dataclasses import dataclass


@dataclass
class Entry:
    feed_id: int
    tag_id: str
    name: str
    version: str
    url: str
