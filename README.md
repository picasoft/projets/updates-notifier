# Updates Notifier

Sends an email and a message in Mattermost to alert when new versions of tools are available.

Initial credit goes to Romain De Laage.

## How to install

Let's say you want to install `updates-notifier==X.Y.Z`.

### With Docker/Podman

The container image is based on Alpine linux, and weights arround 90Mo. For a list of available tags, see [here](https://gitlab.utc.fr/picasoft/projets/updates-notifier/container_registry/62).

```bash
# No login is required to pull the image
docker pull registry.gitlab.utc.fr/picasoft/projets/updates-notifier:X.Y.Z
```

The docker path is quite different from the others installation ways, because it is already configured to run as a cron using [supercronic](https://github.com/aptible/supercronic). You can use directly this program by overriding the default container command when using `docker run`.

### With Python wheel

If using PIP (please make sure `pip`, `setuptools`, and `wheel` packages are up to date):

```bash
pip install updates-notifier==X.Y.Z --extra-index-url https://gitlab.utc.fr/api/v4/projects/10065/packages/pypi/simple
```

### Manual install from source

1) Install and configure [poetry](https://python-poetry.org/docs/#installation) if you don’t have it already :
    ```bash
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
    ```
3) Checkout the tag associated with the version if you want a specific one
    ```bash
    git checkout X.Y.Z
    ```
2) Run `poetry install` in your shell. It should create a `virtualenv` and install all dependencies in it. To activate the virtualenv, run `poetry shell`.

> Note that this project requires at least Python 3.10, but may work with older versions by adjusting the dependencies' version.

## How to use

```
$ notify-updates.py --help
Usage: notify-updates.py [OPTIONS] FEEDS_CFG DB_PATH

  FEEDS_CFG:  Path to the YAML file containing the feeds' parameters
  DB_PATH: Path to the SQLite3 database file

Options:
  -d, --dry-run    Search for new updates, but no db updates or
                   notifications
  -v, --verbose    Toggle debug output
  --no-email       Do not send email
  --no-mattermost  Do not send a Mattermost message
  --help           Show this message and exit.
```

You will also need to setup the following environment variables :

Name | Description
---|---
`MAIL_HOST` | SMTP server host
`MAIL_PORT` | SMTP server port
`MAIL_USER` | SMTP user
`MAIL_PASSWORD` | SMTP password associated to `$MAIL_USER`
`MAIL_SUBJECT` | Subject of the notification email
`MAIL_TO` | Recipients of the notification email
`MAIL_FROM` | Sender of the notification email
`MM_SERVER_HOST` | Mattermost server hostname
`MM_CHANNEL_ID` | Mattermost channel ID where to send the notification
`MM_TOKEN` | Mattermost access token

A [working example of configuration](./config_sample.yaml) is available in this repository.

## How to build the Python wheel

Simply run `poetry build`. The standard wheel with an auto-generated old-school `setup.py` is then available in the `dist/` folder.